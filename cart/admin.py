from django.contrib import admin
from . import models

class CartAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "created_date",
        "update_date",
    ]
    class Meta:
        models = models.Cart
    list_filter = ('user',)

class BookInCartAdmin (admin.ModelAdmin):
    list_display = [
        "cart",
        "quantity",
        "created_date",
        "update_date",
    ]
    class Meta:
        models = models.BookInCart

admin.site.register(models.Cart, CartAdmin)
admin.site.register(models.BookInCart, BookInCartAdmin)