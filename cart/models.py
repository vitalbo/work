from django.db import models
from django.contrib.auth import get_user_model
from books.models import Book
from django.urls import reverse_lazy

User = get_user_model()
class Cart (models.Model):
    user = models.ForeignKey (User, verbose_name= "Покупатель", blank= True, null=True, on_delete= models.PROTECT)
    created_date = models.DateTimeField ("Дата создания", auto_now=False, auto_now_add=True)
    update_date = models.DateTimeField ("Дата изменения", auto_now=True, auto_now_add=False)
    def __str__(self):
        return "Корзина_{} покупателя{}".format(self.pk, self.user)
    def get_absolute_url(self):
        pass
    @property
    def count_book(self):
        total = 0
        for product in self.books_in_cart.all():
            total += product.quantity
        return total
    @property
    def total_price(self):
        total = 0
        for product in self.books_in_cart.all():
            total += product.price_total
        return total
    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'

class BookInCart (models.Model):
    cart = models.ForeignKey (Cart, verbose_name="Продукт в корзине", related_name = "books_in_cart", on_delete=models.CASCADE)
    book = models.ForeignKey (Book, on_delete = models.CASCADE)
    quantity = models.IntegerField ("Количество")
    created_date = models.DateTimeField ("Дата создания", auto_now=False, auto_now_add=True)
    update_date  = models.DateTimeField ("Дата изменения", auto_now=True, auto_now_add=False)

    def __str__(self):
        return "Корзина_{}, покупателя_{}".format(self.pk, self.cart.user)

    @property
    def price_total(self):
        return self.book.price * self.quantity


    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Товар в корзине'
        verbose_name_plural = 'Товары в корзине'
        unique_together = (('cart','book'))

