from django import forms
from .models import BookInCart
from django.forms import ModelForm


class AddBookForm (forms.ModelForm):
    class Meta:
        model = BookInCart
        fields = ['book','quantity']
        widgets = {'book': forms.HiddenInput(),}