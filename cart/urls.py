from  django.conf.urls import url
from django.urls import path
from  . import views
from django.conf import settings
from django.conf.urls import static
urlpatterns = [
    path('<int:pk>/', views.AddBookToCartView.as_view(), name='Cart_upd'),
    path('', views.CartView.as_view(), name='view_cart'),
    path('delete_book/<int:pk>', views.DeleteBookCart.as_view(), name='delete_book'),
]