from django.shortcuts import render
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView
from django.views.generic import DeleteView
from .models import Cart, BookInCart
from order.forms import CheckForm
from .forms import AddBookForm
from books.models import Book
from order.models import OrderStatus
from django.urls import reverse_lazy

new_status = OrderStatus.objects.get (pk=1)

class AddBookToCartView (UpdateView):
    model = BookInCart
    form_class = AddBookForm
    template_name = 'cart/add_book.html'



    def get_object(self):
        cart_id = self.request.session.get ('cart_id')
        if self.request.user.is_anonymous:
            user = None
        else:
            user = self.request.user
        cart, created = Cart.objects.get_or_create (pk = cart_id, defaults= {'user':user})
        self.request.session ['cart_id'] = cart.pk
        book_pk = self.kwargs.get('pk')
        book = Book.objects.get (pk = book_pk)
        book_in_cart, created = self.model.objects.get_or_create(
            cart =cart,
            book = book,
            defaults={'quantity': 1}
        )
        if not created:
            book_in_cart.quantity += 1
            book_in_cart.save()
        return book_in_cart

    def get_context_data(self, **kwargs):
        contex = super().get_context_data(**kwargs)
        contex ['next'] = self.request.GET.get('next', '/')

        return contex

    def get_success_url(self):
        return self.request.POST.get ('next','/')

class CartView (DetailView):
    model = Cart
    template_name = 'cart/view_cart.html'
    def get_object (self):
       cart_id = self.request.session.get("cart_id")
       if self.request.user.is_anonymous:
           user = None
       else:
           user = self.request.user
       cart, cart_created = Cart.objects.get_or_create (pk = cart_id, defaults= {'user':user})
       return cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        checkout_form = CheckForm()
        checkout_form.fields["cart"].initial = self.object
        checkout_form.fields["status"].initial = new_status
        context["form"] = checkout_form
        return context


class DeleteBookCart(DeleteView):
    model = BookInCart
    template_name = 'cart/delete_book.html'

    def get_success_url(self):
        return reverse_lazy('view_cart')