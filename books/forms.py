from django import forms
from django.forms import ModelForm
from .models import Book, Author, Series, Genre, Publish, Binding

class SearchForm (forms.Form):
    search = forms.CharField (label="Поиск")
    active = forms.BooleanField (label="Активный")


class BookCreateForm(ModelForm):
    class Meta:
        model = Book
        fields = ['name','image','price','author','series','genre','year','pages','binding','format','isbn','weight',
                  'age_restr','publish','quantity','active','rate']

class AuthorCreateForm(ModelForm):
    class Meta:
        model = Author
        fields = ['name','year_birth','сountry_bith']

class SerieCreateForm(ModelForm):
    class Meta:
        model = Series
        fields = ['name']

class GenreCreateForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name']

class PublishCreateForm(ModelForm):
    class Meta:
        model = Publish
        fields = ['name']

class BindingCreateForm(ModelForm):
    class Meta:
        model = Binding
        fields = ['name']

class BookUpdateForm(ModelForm):
    class Meta:
        model = Book
        fields = ['name','price','author','series','genre','year','pages','binding','format','isbn','weight',
                  'age_restr','publish','quantity','active','rate']

class AuthorUpdateForm(ModelForm):
    class Meta:
        model = Author
        fields = ['name','year_birth','сountry_bith']

class SerieUpdateForm(ModelForm):
    class Meta:
        model = Series
        fields = ['name']

class GenreUpdateForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name']

class PublishUpdateForm(ModelForm):
    class Meta:
        model = Publish
        fields = ['name']

class BindingUpdateForm(ModelForm):
    class Meta:
        model = Binding
        fields = ['name']


