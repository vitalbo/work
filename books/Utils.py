﻿from books.models import Author, Series, Genre, Publish, Binding
def create_author(obj):
    A = Author (name = obj['name'], year_birth = obj['year_birth'], сountry_bith = obj['сountry_bith'])
    A.save()
    print ('Object created with pk ={}'.format(A.pk))

def create_series (obj):
    S = Series (name = obj['name'])
    S.save()
    print('Object created with pk ={}'.format(S.pk))
def create_genre (obj):
    G = Genre (name = obj['name'])
    G.save()
    print ('Object created with pk ={}'.format(G.pk))
def create_publish(obj):
    P = Publish (name = obj['name'])
    P.save()
    print ('Object created with pk ={}'.format(P.pk))
def create_binding (obj):
    B = Binding (name = obj['name'])
    B.save()
    print ('Object created with pk ={}'.format(B.pk))
#def create_book(obj):

# пример:
# from books.utils import create_author
# Auth  = {'name':'Вася','year_birth':'1981','сountry_bith' :'Россия'}
# create_author (Auth)

def del_series (obj):
    S = Series.objects.get (pk = obj['pk'])
    S.delete()

# пример:
#from books.utils import del_series
#obj ={'pk': '20'}
#del_series (obj)

def Q_auth ():
    A = Author.objects.all()
    S = Series.objects.all()
    G = Genre.objects.all()
    P = Publish.objects.all()
    B = Binding.objects.all()
    print ('Authors - {}'.format(len(A)),'Series -{}'.format(len(S)), 'Genres -{}'.
           format(len(G)),'Publish -{}'.format(len(P)),'Binding -{}'.format(len(B)))

# выполнение:
#from books.utils import Q_auth
#Q_auth()

def add_authors():
    for i in range(25):
        if i % 2:
            Author.objects.create(name = ('Петр {}'.format(i)),year_birth = 1700 + i**2,сountry_bith = 'Россия')
        elif i % 5:
            Author.objects.create(name = ('Николай {}'.format(i)),year_birth = 1850 + i*3,сountry_bith = 'Беларусь')
        else:
            Author.objects.create(name =('Иван {}'.format(i)),year_birth = 1750 + i*5,сountry_bith = 'Украина')

# выполнение:
# from books.utils import add_authors
# add_authors()

def Q_auth_pers():
    A = Author.objects.all()
    F = A.filter(name__contains='Николай')
    print('количество имен Николай = {}'.format(len(F)))

# выполнение:
# from books.utils import Q_auth_pers
# Q_auth_pers()

#def add_book(obj):

def upd_or_create(obj,def_obj):
    G, created = Genre.objects.update_or_create(name=obj['name'],  defaults=def_obj)
    print('Object update or created pk ={}'.format(G.pk))

# выполнение:
# from books.utils import upd_or_create
# upd_or_create({'name': 'Боевик'},{'name': 'Блокбастер'})

def related_book(i):
    B = Book.objects.select_related('boooks').get(pk=3)
    print(B)
