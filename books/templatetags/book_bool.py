from  django import template
register = template.Library()
@register.filter
def boolparser (value):
    if value:
        return "Да"
    return "Нет"

