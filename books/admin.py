from django.contrib import admin
from . import models


class BookAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "created_date",
        "change_date"
    ]
    class Meta:
        model = models.Book

class AuthorAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        ]
    class Meta:
        model = models.Author

class SeriesAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]
    class Meta:
        model = models.Series

class GenreAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]
    class Meta:
        model = models.Genre

class PublishAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]
    class Meta:
        model = models.Publish

class BindingAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]
    class Meta:
        model = models.Binding

admin.site.register(models.Book, BookAdmin)
admin.site.register(models.Author, AuthorAdmin)
admin.site.register(models.Series, SeriesAdmin)
admin.site.register(models.Genre, GenreAdmin)
admin.site.register(models.Publish, PublishAdmin)
admin.site.register(models.Binding, BindingAdmin)