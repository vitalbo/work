from  django.conf.urls import url
from django.urls import path
from  . import views
from django.conf import settings
from django.conf.urls import static
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('book/', views.BookList.as_view(), name='Book_list'),
    path('author/', views.AuthorList.as_view(), name='Author_list'),
    path('series/', views.SeriesList.as_view(), name='Series_list'),
    path('genre/', views.GenreList.as_view(), name='Genre_list'),
    path('publish/', views.PublishList.as_view(), name='Publish_list'),
    path('binding/', views.BindingList.as_view(), name='Binding_list'),
    path('book/<int:pk>/', views.BookDetail.as_view(), name='Book_detail'),
    path('author/<int:pk>/', views.AuthorDetail.as_view(), name='Author_detail'),
    path('series/<int:pk>/',views.SerieDetail.as_view(), name='Series_detail'),
    path('genre/<int:pk>/',views.GenreDetail.as_view(), name='Genre_detail'),
    path('publish/<int:pk>/', views.PublishDetail.as_view(), name='Publish_detail'),
    path('binding/<int:pk>/', views.BindingDetail.as_view(), name='Binding_detail'),
    path('book/add/', views.BookCreate.as_view(), name='Book_add'),
    path('author/add/', views.AuthorCreate.as_view(), name='Author_add'),
    path('series/add/', views.SerieCreate.as_view(), name='Series_add'),
    path('genre/add/', views.GenreCreate.as_view(), name='Genre_add'),
    path('publish/add/', views.PublishCreate.as_view(), name='Publish_add'),
    path('binding/add/', views.BindingCreate.as_view(), name='Binding_add'),
    path('book/<int:pk>/update', views.BookUpdate.as_view(), name='Book_upd'),
    path('author/<int:pk>/update', views.AuthorUpdate.as_view(), name='Author_upd'),
    path('series/<int:pk>/update', views.SerieUpdate.as_view(), name='Series_upd'),
    path('genre/<int:pk>/update', views.GenreUpdate.as_view(), name='Genre_upd'),
    path('publish/<int:pk>/update', views.PublishUpdate.as_view(), name='Publish_upd'),
    path('binding/<int:pk>/update', views.BindingUpdate.as_view(), name='Binding_upd'),
    path('book/<int:pk>/delete', views.BookDelete.as_view(), name='Book_del'),
    path('author/<int:pk>/delete', views.AuthorDelete.as_view(), name='Author_del'),
    path('series/<int:pk>/delete', views.SerieDelete.as_view(), name='Series_del'),
    path('genre/<int:pk>/delete', views.GenreDelete.as_view(), name='Genre_del'),
    path('publish/<int:pk>/delete', views.PublishDelete.as_view(), name='Publish_del'),
    path('binding/<int:pk>/delete', views.BindingDelete.as_view(), name='Binding_del'),
    path('ref/', views.RefList, name="Reference"),
    path('main/', views.MainList, name="MainList")
]