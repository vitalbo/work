from django.db import models
from django.urls import reverse_lazy

class Book(models.Model):
    name = models.CharField("Название", max_length=70, db_index=True)
    image = models.ImageField ("Обложка",upload_to='blah')
    price = models.DecimalField ("Цена", max_digits=7, decimal_places=2)
    author = models.ManyToManyField("books.Author", related_name= 'boooks', verbose_name="Автор")
    series = models.ForeignKey("books.Series",related_name= 'boooks', verbose_name="Серия", on_delete=models.PROTECT,null=True, blank=True)
    genre = models.ManyToManyField("books.Genre",  related_name = 'boooks',verbose_name= "Жанр")
    year = models.IntegerField ("Год выпуска")
    pages = models.IntegerField ("Количество страниц")
    binding = models.ForeignKey ("books.Binding", related_name= 'boooks', verbose_name="Переплёт", on_delete=models.PROTECT)
    format = models.CharField ("Формат", max_length = 50)
    isbn = models.IntegerField ("Уникальный номер")
    weight = models.IntegerField ("Вес")
    age_restr = models.CharField ("Возрастные ограничения", max_length = 50)
    publish  = models.ForeignKey( "books.Publish", related_name= 'boooks', verbose_name="Издательство", on_delete=models.PROTECT)
    quantity = models.IntegerField ("Кол-во книг в налиичии")
    active = models.BooleanField("Доступна для заказ", default=True)
    rate = models.IntegerField ("Рейтинг", default=0)
    created_date = models.DateTimeField("Дата создания", auto_now=False, auto_now_add=True)
    change_date = models.DateTimeField("Дата изменения", auto_now=True, auto_now_add=False)
    comment = models.CharField ('Комментарии',default='', max_length = 1000)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'
        ordering = ('name',)

class Author (models.Model):
    name = models.CharField ("ФИО", max_length=70, db_index= True)
    year_birth = models.IntegerField ("Год рождения",null=True, blank=True)
    сountry_bith  = models.CharField ("Страна рождения", max_length=30)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'
        ordering = ('name',)

class Series (models.Model):
    name = models.CharField("Название", max_length=50, db_index=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Серия'
        verbose_name_plural = 'Серии'
        ordering = ('name',)

class Genre (models.Model):
    name = models.CharField("Название", max_length=50, db_index=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'
        ordering = ('name',)

class Publish (models.Model):
    name = models.CharField("Название",  max_length=50, db_index=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Издательство'
        verbose_name_plural = 'Издательства'
        ordering = ('name',)

class Binding (models.Model):
    name = models.CharField("Название", max_length=50, db_index=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        pass
    class Meta:
        verbose_name = 'Переплёт'
        verbose_name_plural = 'Переплёты'
        ordering = ('name',)
