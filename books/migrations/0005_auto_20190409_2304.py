# Generated by Django 2.2 on 2019-04-09 20:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0004_auto_20190409_2300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='year_birth',
            field=models.IntegerField(blank=True, null=True, verbose_name='Год рождения'),
        ),
        migrations.AlterField(
            model_name='book',
            name='series',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='boooks', to='books.Series', verbose_name='Серия'),
        ),
    ]
