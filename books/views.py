from django.shortcuts import render
from django.template.response import TemplateResponse
from .models import Book, Author, Series, Genre, Publish, Binding
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from .forms import AuthorCreateForm, BookCreateForm, SerieCreateForm, GenreCreateForm, PublishCreateForm, BindingCreateForm
from .forms import AuthorUpdateForm, BookUpdateForm, SerieUpdateForm, GenreUpdateForm, PublishUpdateForm, BindingUpdateForm
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required

class BookDetail (DetailView):
    model = Book
    form_class = BookCreateForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)

class AuthorDetail (PermissionRequiredMixin,DetailView):
    model = Author
    permission_required = 'books.add_series'
    form_class = AuthorCreateForm
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)

class SerieDetail (PermissionRequiredMixin,DetailView):
    model = Series
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)

class GenreDetail (PermissionRequiredMixin,DetailView):
    model = Genre
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)

class PublishDetail (PermissionRequiredMixin,DetailView):
    model = Publish
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)

class BindingDetail (PermissionRequiredMixin,DetailView):
    model = Binding
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)

class BookList(ListView):
    model = Book
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)
    def get_queryset(self, **kwargs):
        qs = super().get_queryset(**kwargs)
        query = self.request.GET.get('q')
        if query:
            object_list = qs.filter(Q(name__startswith=query) | Q(author__name__startswith=query))
        else:
            object_list = self.model.objects.all()
        return object_list

class AuthorList (PermissionRequiredMixin,ListView):
    model = Author
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)
    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(name__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

class SeriesList (PermissionRequiredMixin,ListView):
    model = Series
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)
    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(name__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

class GenreList (PermissionRequiredMixin,ListView):
    model = Genre
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)
    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(name__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

class PublishList(PermissionRequiredMixin,ListView):
    model = Publish
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)
    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(name__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

class BindingList (PermissionRequiredMixin,ListView):
    model = Binding
    permission_required = 'books.add_series'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return (context)
    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(name__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

class BookCreate (PermissionRequiredMixin,CreateView):
    model = Book
    permission_required = 'books.add_series'
    form_class = BookCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Book_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Book_list')


class AuthorCreate (PermissionRequiredMixin,CreateView):
    model = Author
    permission_required = 'books.add_series'
    form_class = AuthorCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Author_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Author_list')

class SerieCreate (PermissionRequiredMixin,CreateView):
    model = Series
    permission_required = 'books.add_series'
    form_class = SerieCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Series_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Series_list')

class GenreCreate (PermissionRequiredMixin,CreateView):
    model = Genre
    permission_required = 'books.add_series'
    form_class = GenreCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Genre_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Genre_list')

class PublishCreate (PermissionRequiredMixin,CreateView):
    model = Publish
    permission_required = 'books.add_series'
    form_class = PublishCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Publish_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Publish_list')

class BindingCreate (PermissionRequiredMixin,CreateView):
    model = Binding
    permission_required = 'books.add_series'
    form_class = BindingCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Binding_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Binding_list')

class BookUpdate (PermissionRequiredMixin,UpdateView):
    model = Book
    permission_required = 'books.add_series'
    form_class = BookCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Book_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Book_list')


class AuthorUpdate (PermissionRequiredMixin,UpdateView):
    model = Author
    permission_required = 'books.add_series'
    form_class = AuthorCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Author_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Author_list')

class SerieUpdate (PermissionRequiredMixin,UpdateView):
    model = Series
    permission_required = 'books.add_series'
    form_class = SerieUpdateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Series_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Series_list')

class GenreUpdate (PermissionRequiredMixin,UpdateView):
    model = Genre
    permission_required = 'books.add_series'
    form_class = GenreCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Genre_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Genre_list')

class PublishUpdate (PermissionRequiredMixin,UpdateView):
    model = Publish
    permission_required = 'books.add_series'
    form_class = PublishCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Publish_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Publish_list')

class BindingUpdate(PermissionRequiredMixin,UpdateView):
    model = Binding
    permission_required = 'books.add_series'
    form_class = BindingCreateForm
    def get_success_url(self):
       redirect = self.request.POST.get('detail')
       if redirect:
           return reverse_lazy ('Binding_detail', kwargs = {'pk':self.object.pk})
       return reverse_lazy ('Binding_list')

class BookDelete (PermissionRequiredMixin,DeleteView):
    model = Book
    permission_required = 'books.add_series'
    success_url = reverse_lazy ("Book_list")

class AuthorDelete (PermissionRequiredMixin,DeleteView):
    model = Author
    permission_required = 'books.add_series'
    success_url = reverse_lazy ("Author_list")

class SerieDelete (PermissionRequiredMixin,DeleteView):
    model = Series
    permission_required = 'books.add_series'
    success_url = reverse_lazy ("Series_list")

class GenreDelete (PermissionRequiredMixin,DeleteView):
    model = Genre
    permission_required = 'books.add_series'
    success_url = reverse_lazy ("Book_list")

class PublishDelete (PermissionRequiredMixin,DeleteView):
    model = Publish
    permission_required = 'books.add_series'
    success_url = reverse_lazy ("Author_list")

class BindingDelete (PermissionRequiredMixin,DeleteView):
    model = Binding
    permission_required = 'books.add_series'
    success_url = reverse_lazy ("Series_list")

permission_required('books.add_series')
def RefList(request):
    A_list = "Список авторов"
    S_list = "Список серий"
    G_list = "Список жанров"
    P_list = "Список издательств"
    B_list = "Список переплетов"
    data = {"A_list": A_list, "S_list": S_list, "G_list": G_list, "P_list": P_list, "B_list": B_list}
    return render(request, "books/reference.html", context=data)

#@permission_required()
def MainList(request):
        Book_list = "Список книг"
        data = {"Book_list": Book_list}
        return render(request, "books/main.html", context=data)

