from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from .models import Order, OrderStatus
from .forms import CheckForm
from django.urls import reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin



class OrderView(CreateView):
    model = Order
    template_name = 'cart/view_cart.html'
    form_class = CheckForm

    def get_success_url(self):
        del self.request.session['cart_id']
        return reverse_lazy('order_success', kwargs={'pk': self.object.pk})


class OrderSuccess(DetailView):
    model = Order
    template_name = 'order/order.html'

class OrderListPers(PermissionRequiredMixin, ListView):
    model = Order
    template_name = 'order/order_list_pers.html'
    permission_required = 'books.view_series'

class OrderListCust(ListView):
    model = Order
    template_name = 'order/order_list_cust.html'


class OrderUpdatePers(PermissionRequiredMixin, UpdateView):
    model = Order
    template_name = 'order/order_update_pers.html'
    fields = ['status']
    permission_required = 'books.view_series'

    def get_success_url(self):
        return reverse_lazy('order_list_pers')

class OrderUpdateCust(UpdateView):
    model = Order
    template_name = 'order/order_update_cust.html'
    form_class = CheckForm
    def get_success_url(self):
        return reverse_lazy('order_list_cust')


class OrderCancelCust(UpdateView):
    model = Order
    template_name = 'order/order_cancel_cust.html'
    fields = ['status']
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #checkout_form = CheckForm()
        new_status = OrderStatus.objects.get(pk=4)
        #checkout_form.fields["status"].initial = new_status
        context["status"] = new_status
        return context
    def get_success_url(self):
        return reverse_lazy('order_list_cust')
