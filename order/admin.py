from django.contrib import admin

from django.contrib import admin
from . import models

class OrderAdmin(admin.ModelAdmin):
    list_display = [
        "cart","created_day", 'status',
    ]
class OrderStatusAdmin(admin.ModelAdmin):
    list_display = [
        "status",
    ]


admin.site.register(models.Order, OrderAdmin)
admin.site.register(models.OrderStatus, OrderStatusAdmin)
