from django.db import models
from cart.models import Cart

class Order(models.Model):
    cart = models.ForeignKey(Cart,on_delete=models.PROTECT)
    status = models.ForeignKey("order.OrderStatus", verbose_name="Статус заказа", on_delete=models.PROTECT)
    city = models.CharField( "Город",null=True,blank=True,max_length=15)
    street = models.CharField("Улица",null=True,blank=True,max_length=40)
    building = models.CharField("Дом",null=True,blank=True,max_length=7)
    flat = models.CharField("Квартира",null=True,blank=True,max_length=7)
    email = models.EmailField(verbose_name="Электронная почта",null=True,blank=True)
    phone = models.CharField(verbose_name="Контактный телефон", max_length=20)
    created_day = models.DateTimeField("Дата создания заказа",auto_now=False,auto_now_add=True)
    updated_date = models.DateTimeField("Дата последнего изменения",auto_now=True,auto_now_add=False)
    comment = models.CharField('Комментарии',  default='', max_length=1000)
    def __str__(self):
        return "Заказ № {}".format(self.cart.pk)
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

class OrderStatus(models.Model):
    status = models.CharField("Статус", max_length=50)
    def __str__(self):
        return self.status
    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'