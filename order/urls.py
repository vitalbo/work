
from django.urls import path
from order.views import *

urlpatterns = [
    path('order', OrderView.as_view(), name='order_create'),
    path('order/<int:pk>', OrderSuccess.as_view(), name='order_success'),
    path('order-list-pers', OrderListPers.as_view(), name='order_list_pers'),
    path('order-update-pers/<int:pk>', OrderUpdatePers.as_view(), name='order_update_pers'),
    path('order-list-cust', OrderListCust.as_view(), name='order_list_cust'),
    path('order-update-cust/<int:pk>', OrderUpdateCust.as_view(), name='order_update_cust'),
    path('order-cancel-cust/<int:pk>', OrderCancelCust.as_view(), name='order_cancel_cust'),
]