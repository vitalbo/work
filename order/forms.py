from django import forms
from django.forms import ModelForm
from .models import *


class CheckForm(ModelForm):
    class Meta:
        model = Order
        fields = ['cart', 'status', 'city', 'street', 'building', 'flat', 'email',
                  'phone']
        widgets = {'cart': forms.HiddenInput, 'status': forms.HiddenInput}