from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth import login, get_user_model
from django.contrib.auth.views import LoginView,LogoutView, PasswordResetView, PasswordResetConfirmView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from cart.models import User
from .forms import CreateUserForm, UpdateUserForm
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import UserPassesTestMixin


class LogView (LoginView):
   template_name = 'log_auth/login.html'

class LogOutView (LogoutView):
   template_name = 'log_auth/logout.html'

class CreateUser(CreateView):
   model = User
   template_name = 'log_auth/create_user.html'
   form_class = CreateUserForm

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      context['next'] = self.request.GET.get('next', '/')
      return context

   def get_success_url(self):
      self.object.set_password(self.object.password)
      self.object.save()
      return self.request.POST.get('next', '/')


class UpdateUser(UserPassesTestMixin, UpdateView):
   model = User
   template_name = "log_auth/update_user.html"
   form_class = UpdateUserForm

   def get_success_url(self):
      print(self.object)
      return reverse_lazy('view-user', kwargs={'pk': self.object.pk})

   def test_func(self):
      return self.request.user.pk == self.kwargs.get('pk')


class ViewUser(UserPassesTestMixin, DetailView):
   model = User
   template_name = "log_auth/user.html"

   def test_func(self):
      return self.request.user.pk == self.kwargs.get('pk')

class PasswordResetView(PasswordResetView):
   template_name = 'log_auth/password_reset_form.html'
   email_template_name = 'log_auth/password_reset_email.html'
   success_url = '/'

class PasswordResetConfirmView(PasswordResetConfirmView):
   template_name = 'log_auth/password_reset_confirm.html'
   success_url = '/'