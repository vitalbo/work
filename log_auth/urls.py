from django.urls import path
from log_auth.views import *

app_name = 'auth'
urlpatterns = [
    path('login', LogView.as_view(), name='log_in'),
    path('logout', LogOutView.as_view(), name='log_out'),
    path('create-user', CreateUser.as_view(), name='create_user'),
    path('update-user/<int:pk>', UpdateUser.as_view(), name='update_user'),
    path('view-user/<int:pk>', ViewUser.as_view(), name='view_user'),
    path('password-reset', PasswordResetView.as_view(), name='password_reset'),
    path('password-reset-done', PasswordResetView.as_view(), name='password_reset'),
    path('password-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
         PasswordResetView.as_view(), name='password_reset_confirm'),
]